require "healthplannercalculator/version"

module Healthplannercalculator

	class Calculator
	
		def self.calorie_calc(gender, weight, height, age)

			if gender=="M"
			   	result = (10*weight)+(6.25*height)-(5*age)+5
			   	@result1=result
			else
			   	result = (10*weight)+(6.25*height)-(5*age)-161
			   	@result1=result
			end

			return @result1 
		end

		def self.protien_calc(calorie_intake)
			cal_intake = calorie_intake.to_f
			result = (cal_intake * 0.20) / 4    # calculating protein intake from calorie intake
			@result2 = result

			return @result2
		end
	end
end
